<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              aldofieuw.com
 * @since             1.0.0
 * @package           Woototalvariations
 *
 * @wordpress-plugin
 * Plugin Name:       WooTotal & Variations
 * Plugin URI:        aldofieuw.com
 * Description:       Shows total amount sold by product
 * Version:           1.0.0
 * Author:            Aldo
 * Author URI:        aldofieuw.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woototalvariations
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('WOOTOTALVARIATIONS_VERSION', '1.0.1');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woototalvariations-activator.php
 */
function activate_woototalvariations()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-woototalvariations-activator.php';
	Woototalvariations_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woototalvariations-deactivator.php
 */
function deactivate_woototalvariations()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-woototalvariations-deactivator.php';
	Woototalvariations_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_woototalvariations');
register_deactivation_hook(__FILE__, 'deactivate_woototalvariations');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-woototalvariations.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woototalvariations()
{

	$plugin = new Woototalvariations();
	$plugin->run();
}
run_woototalvariations();
