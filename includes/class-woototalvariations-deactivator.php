<?php

/**
 * Fired during plugin deactivation
 *
 * @link       aldofieuw.com
 * @since      1.0.0
 *
 * @package    Woototalvariations
 * @subpackage Woototalvariations/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woototalvariations
 * @subpackage Woototalvariations/includes
 * @author     Aldo <info@aldofieuw.com>
 */
class Woototalvariations_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
