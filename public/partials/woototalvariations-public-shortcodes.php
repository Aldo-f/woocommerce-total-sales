<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       aldofieuw.com
 * @since      1.0.0
 *
 * @package    Woototalvariations
 * @subpackage Woototalvariations/public/partials
 */


/**
 * Register
 */
function register_shortcodes()
{
    add_shortcode('recent-posts', 'recent_posts_shortcode');
    add_shortcode('wc-show-total', 'wc_show_total_shortcode');
}

/**
 * Init
 */
add_action('init', 'register_shortcodes');
// add_action('woocommerce_single_product_summary', 'wc_show_total_shortcode', 11);
// add_action('woocommerce_before_add_to_cart_form', 'wc_show_total_shortcode', 11); // uncomment to show on product page (will be visible on every product-page)
// add_action('woocommerce_before_variations_form', 'wc_show_total_shortcode', 11);
// add_action('woocommerce_before_add_to_cart_button', 'wc_show_total_shortcode', 11);
// add_action('woocommerce_before_single_variation', 'wc_show_total_shortcode', 11);
// add_action('woocommerce_single_variation', 'wc_show_total_shortcode', 11);

/**
 * Shortcodes
 */
function recent_posts_shortcode($atts, $content = null)
{

    // Attributes
    $atts = shortcode_atts(
        array(
            'posts' => '5',
        ),
        $atts,
        'recent-posts'
    );

    // Query
    $the_query = new WP_Query(array('posts_per_page' => $atts['posts']));

    // Posts
    $output = '<ul>';
    while ($the_query->have_posts()) :
        $the_query->the_post();
        $output .= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
    endwhile;
    $output .= '</ul>';

    // Reset post data
    wp_reset_postdata();

    // Return code
    return $output;
}

function wc_show_total_shortcode($atts)
{
    if (!class_exists('WC_Product_Factory')) {
        // error: is woocommerce active
        print_r('WooCommerce is not active');
        return;
    }

    $_pf = new WC_Product_Factory();
    if (!empty($atts['product'])) {
        $products = explode(',', $atts['product']);

        foreach ($products as $id) {
            $_product = $_pf->get_product($id);

            if (empty($_product)) continue;
            $_products[] = $_product;
            // showData($_product);
        }

        if (!empty($_products)) {
            $data = get_total_sales_products($_products, $atts);
        }
    } else {
        global $product;
        if (empty($product)) return;
        // showData($product);
        $data = get_total_sales_products([$product], $atts);
    }

    if (!empty($data))
        displayData($data);
}


// show product
function displayData($data)
{
    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';

    $output = "";
    foreach ($data as $key => $item) {

        if (!empty($item['parent'])) {

            $output .= '<ul>';
            $output .= "<li><a href='" . $item['parent']['url'] .  "'>" . $item['parent']['name'] . '</a>';

            $total_sales = array_column($item['children'], 'total_sales');
            array_multisort($total_sales, SORT_DESC, $item['children']);

            if (!empty($item['children'])) {
                $output .= '<ol>';
                foreach ($item['children'] as $key => $value) {
                    /*    $percent = ($item['parent']['total_sales'] > 0) ? number_format($value['total_sales'] / $item['parent']['total_sales'] * 100, 1, ',', '')  : 0;*/
                    $output .= '<li>' . $value['name'] . ': ' . $value['total_sales'] . '</li>';
                }
                $output .= '</ol>';
            }

            $output .= '</li>';
            $output .= '</ul>';
        }
    }

    echo $output;
}

function get_total_sales_products($products, $atts)
{
    $data = [];

    foreach ($products as $key => $product) {
        get_total_sales_from_product_or_variation($product->get_id());

        if ($product->is_type("variable")) {

            $data[$key]['parent'] = [
                'name'          => $product->get_name(),
                'SKU'           => $product->get_sku(),
                'total_sales'   => $product->get_total_sales(),
                'url'           => get_site_url() . '/' . $product->get_slug(),
            ];

            $variation_data = get_all_product_variations($product->get_children(false), $atts);

            foreach ($product->get_children(false) as $c_key => $child_id) {
                // get an instance of the WC_Variation_product Object
                $variation = wc_get_product($child_id);

                if (!$variation || !$variation->exists()) {
                    continue;
                }
                $attributes = $variation->get_attributes();
                $variation_key = array_search($child_id, array_column($variation_data, 'variation_id'));
                $total_sales = !empty($variation_data[$variation_key]['total_sales']) ? $variation_data[$variation_key]['total_sales'] : 0;

                $data[$key]['children'][] = [
                    // 'name'          => $variation->get_name(),
                    'name'          => implode(', ', $attributes),
                    'SKU'           => $variation->get_sku(),
                    'total_sales'   => $total_sales,
                    'url'           => get_site_url() . '/' . $variation->get_slug(),
                ];
            }
        } else {
            // product without variations
            $data[$key] = [
                'name'          => $product->get_name(),
                'SKU'           => $product->get_sku(),
                'total_sales'   => $product->get_total_sales(),
                'url'           => get_site_url() . '/' . $product->get_slug(),
            ];
        }
    }

    return $data;
}


function get_all_product_variations($variations, $atts)
{
    $variations = implode(', ', $variations);
    global $wpdb;

    $prefix = $wpdb->prefix;
    $order_product = $prefix . 'wc_order_product_lookup';
    $order_stats = $prefix . 'wc_order_stats';
    $valid_stats = "'wc-processing', 'wc-pending', 'wc-on-hold', 'wc-completed'";

    $q_start_date = '';
    $start_date = $atts['start-date'] ?? null;

    if (!empty($atts['start-date'])) {
        // TODO: validate date as a correct formatted date
        $start_date = $atts['start-date'];
        $q_start_date = "AND order_product.date_created >= '$start_date'";
    }


    $phoen_product_query = "
        SELECT  
        posts.id, posts.post_name, posts.post_type,
        order_product.variation_id, 
        order_product.date_created,
        SUM(order_product.product_qty) as total_sales

        FROM  {$wpdb->posts} AS posts

        LEFT JOIN $order_product AS order_product
                ON posts.id = order_product.variation_id
        LEFT JOIN $order_stats AS order_stats
                ON order_product.order_id = order_stats.order_id 

        WHERE 
            posts.post_status IN ( 'publish','private' ) 
            AND posts.post_type IN ( 'product','product_variation' ) 
            AND posts.id IN ( $variations )
            AND order_stats.status IN ( $valid_stats )
            $q_start_date 

        GROUP BY 
            posts.id
        ORDER BY
            total_sales DESC
    ";
    // echo '<pre>';
    // print_r($phoen_product_query);
    // echo '</pre>'; 
    $phoen_product_data = $wpdb->get_results($phoen_product_query, ARRAY_A);
    // print_r($phoen_product_data);

    return $phoen_product_data;
}


/****************************************************
 * Works only when updating every variation on sale *
 * **************************************************/

function get_total_sales_from_product_or_variation($product_id)
{
    $product = wc_get_product($product_id);

    if (!$product) {
        return 0;
    }

    $total_sales = is_a($product, 'WC_Product_Variation') ? get_post_meta($product_id, 'total_sales', true) : $product->get_total_sales();

    // print_r($total_sales . ' ');
    return $total_sales;
}


add_action('woocommerce_recorded_sales', 'record_sales_for_variations', 20);

/**
 * Record Sales for Variations
 */
function record_sales_for_variations($order_id)
{
    $order = wc_get_order($order_id);

    if (!$order) {
        return;
    }

    if ($order->get_meta('stas0238_variation_sales_recorded', true)) {
        return;
    }

    if (count($order->get_items()) > 0) {
        foreach ($order->get_items() as $item) {
            $product_id = $item->get_id();
            if ($product_id) {
                $data_store = WC_Data_Store::load('product');
                $data_store->update_product_sales($product_id, absint($item['qty']), 'increase');
            }
        }
    }

    $order->update_meta_data('rps_variation_sales_recorded', true);
}
